/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author User
 */
public class TriangleFrame2 extends JFrame{
    JLabel lblHeight;
    JTextField txtHeight;
    JLabel lblBase;
    JTextField txtBase;
    JButton btnCalculate;
    JLabel lblResult;
    public TriangleFrame2() {
        super("Triangle");
        this.setSize(500, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblHeight = new JLabel("height: ", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(5, 5);
        lblHeight.setBackground(Color.white);
        lblHeight.setOpaque(true);
        this.add(lblHeight);

        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 5);
        this.add(txtHeight);
        
        lblBase = new JLabel("base: ", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setLocation(115, 5);
        lblBase.setBackground(Color.white);
        lblBase.setOpaque(true);
        this.add(lblBase);

        txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(170, 5);
        this.add(txtBase);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(225, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Triangle height = ??? , base = ??? "
                + ", opposite = ??? area = ??? perimater = ???");
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 50);
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strHeight = txtHeight.getText();
                    String strBase = txtBase.getText();
                    double height = Double.parseDouble(strHeight);
                    double base = Double.parseDouble(strBase);
                    Triangle triangle = new  Triangle(height,base);
                    lblResult.setText("Triangle height = " + triangle.getHeight() 
                            + " , base = " + triangle.getBase()
                            + " , opposite = " + String.format("%.2f",triangle.getOpposite())
                            + " area = " + String.format("%.2f", triangle.calArea())
                            + " perimater = " + String.format("%.2f", triangle.calPerimater()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame2.this, "Error : Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtHeight.setText("");
                    txtHeight.requestFocus();
                    txtBase.setText("");
                    txtBase.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        TriangleFrame2 frame = new TriangleFrame2();
        frame.setVisible(true);
    }
}
