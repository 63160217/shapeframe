/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.shapeframe;

/**
 *
 * @author User
 */
public class Triangle extends Shape{
    
    private double height;
    private double base;
    private double opposite;

    public Triangle(double height, double base) {
        super("Triangle");
        this.height = height;
        this.base = base;
        this.opposite = (Math.sqrt((Math.pow(height, 2))+(Math.pow(base, 2))));
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public void setOpposite(double opposite) {
        this.opposite = opposite;
    }

    public double getOpposite() {
        return opposite;
    }

    public double getHeight() {
        return height;
    }

    public double getBase() {
        return base;
    }

    @Override
    public double calArea() {
        return 1/2.0 * height * base;
    }

    @Override
    public double calPerimater() {
        return opposite + height + base;
    }
}
